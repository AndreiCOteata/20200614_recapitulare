import controller.ResourceManager;

public class Application {
    public static void main(String[] args) throws InterruptedException {
        ResourceManager resourceManager = new ResourceManager();
        resourceManager.startRManager();
    }
}
