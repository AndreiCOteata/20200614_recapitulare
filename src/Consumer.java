package model;

import view.GenericData;

import java.util.Queue;

public class Consumer extends Thread{
    private Queue<GenericData> container;
    private Object lock;

    public Consumer(Queue<GenericData> resources, Object lock){
        this.container = resources;
        this.lock = lock;
    }

    public void startConsumer() {
        start();
    }

    @Override
    public void run(){
        while(container.peek() != null){
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                synchronized (lock){
                    if(container.peek() != null) {
                        System.out.println("Consumer: " + this.container.poll());
                    }
                }
            }
        }
    }
}
