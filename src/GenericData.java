package view;

public class GenericData {
    private String dataInfo;

    public GenericData(String dataInfo){
        this.dataInfo = dataInfo;
    }

    @Override
    public String toString(){
        return dataInfo;
    }
}
