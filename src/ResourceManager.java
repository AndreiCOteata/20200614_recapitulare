package controller;

import view.GenericData;
import model.Consumer;
import model.MyProducer;
import model.Producer;

import java.io.Console;
import java.util.LinkedList;
import java.util.Queue;

public class ResourceManager {
    private Queue<GenericData> resources = new LinkedList<>();
    private Producer producer;
    private Consumer consumer;
    private Object lock = new Object();

    public ResourceManager(){
        this.producer = new MyProducer(resources, lock);
        this.consumer = new Consumer(resources, lock);
    }

    public void startRManager() throws InterruptedException {
        this.producer.startProduct(700);
        Thread.sleep(2000);
        this.consumer.startConsumer();
        Consumer consumer1 = new Consumer(resources,lock);
        consumer1.startConsumer();
        Consumer consumer2 = new Consumer(resources,lock);
        consumer2.startConsumer();
        this.consumer.join();
        consumer1.join();
        consumer2.join();
        this.producer.stopProduct();
    }
}
