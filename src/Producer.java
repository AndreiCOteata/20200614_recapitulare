package model;

public interface Producer {
    void startProduct(int frequency);
    void stopProduct();
}
