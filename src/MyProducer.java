package model;

import view.GenericData;
import view.MyData;

import java.util.Queue;
import java.util.Random;

public class MyProducer extends Thread implements Producer {
    private Queue<GenericData> container;
    private int frequency;
    private boolean isRunning = false;
    private Object lock;
    public MyProducer(Queue<GenericData> resources, Object lock){
        this.container = resources;
        this.lock =lock;
    }
    @Override
    public void startProduct(int frequency) {
        this.frequency = frequency;
        this.isRunning = true;
        start();
    }

    @Override
    public void stopProduct() {
        this.isRunning = false;
    }
    @Override
    public void run(){
        while(this.isRunning){
            try {
                Thread.sleep(this.frequency);
            } catch (InterruptedException e) {
                System.out.println("Interrupted Thread");
            }finally {
                MyData myData = this.generateRandom();
                synchronized (lock) {
                    this.container.add(myData);
                }
                System.out.println("My data " + myData + " and has the size of " + container.size());
            }
        }
    }

    private MyData generateRandom(){
        Random random = new Random();
        return new MyData(String.format("random: %s", random.nextInt(100)));
    }
}
